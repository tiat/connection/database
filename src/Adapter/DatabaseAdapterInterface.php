<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter;

//
use Laminas\Db\Adapter\Adapter as LaminasAdapter;
use Laminas\Db\Sql\Sql as LaminasSql;
use Laminas\Db\Sql\SqlInterface;
use Tiat\Connection\Database\Adapter\Driver\DatabaseDriverInterface;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Query\DatabaseQueryInterface;
use Tiat\Connection\Database\Result\DatabaseResultInterface;

/**
 * @version 3.0.1
 * @since   3.0.0 First time introduced.
 */
interface DatabaseAdapterInterface extends DatabaseInterface {
	
	/**
	 * @param    DatabaseSettingsInterface    $settings
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(DatabaseSettingsInterface $settings);
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void;
	
	/**
	 * @param ...$args
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : DatabaseAdapterInterface;
	
	/**
	 * @param ...$args
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : DatabaseAdapterInterface;
	
	/**
	 * @param    DatabaseSettingsInterface    $settings
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettings(DatabaseSettingsInterface $settings) : DatabaseAdapterInterface;
	
	/**
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetSettings() : DatabaseAdapterInterface;
	
	/**
	 * @param    bool    $autoCreate
	 *
	 * @return null|DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriver(bool $autoCreate) : ?DatabaseDriverInterface;
	
	/**
	 * @param    DatabaseDriverInterface    $driver
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDriver(DatabaseDriverInterface $driver) : DatabaseAdapterInterface;
	
	/**
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDriver() : DatabaseAdapterInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionResult() : mixed;
	
	/**
	 * @param    mixed    $result
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionResult(mixed $result) : DatabaseAdapterInterface;
	
	/**
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionResult() : DatabaseAdapterInterface;
	
	/**
	 * Return query interface for database instance.
	 *
	 * @param    string|int    $name
	 *
	 * @return null|DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery(string|int $name) : ?DatabaseQueryInterface;
	
	/**
	 * @param    string|int      $name
	 * @param    array|string    $query
	 * @param    bool            $override
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setQuery(string|int $name, array|string $query, bool $override) : DatabaseAdapterInterface;
	
	/**
	 * @param    array    $queries
	 * @param    bool     $override
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueries(array $queries, bool $override) : DatabaseAdapterInterface;
	
	/**
	 * @param    array    $list
	 * @param    bool     $override
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryInterfaces(array $list, bool $override = FALSE) : DatabaseAdapterInterface;
	
	/**
	 * @param    DatabaseQueryInterface    $query
	 * @param    string|int                $name
	 * @param    bool                      $override
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryInterface(DatabaseQueryInterface $query, string|int $name, bool $override) : DatabaseAdapterInterface;
	
	/**
	 * If adapter does not exist or $new is TRUE then create new instance, otherwise current instance is used.
	 *
	 * @param    bool    $new
	 *
	 * @return null|LaminasAdapter
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryAdapter(bool $new = FALSE) : ?LaminasAdapter;
	
	/**
	 * If adapter does not exist or $new is TRUE then create new instance, otherwise current instance is used.
	 *
	 * @param    bool    $new
	 *
	 * @return null|LaminasSql
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryPlatform(bool $new = FALSE) : ?LaminasSql;
	
	/**
	 * Add Laminas SqlInterface output directly to Adapter query list as interface.
	 *
	 * @param    SqlInterface    $sqlInterface
	 * @param    string          $name
	 *
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addPlatformSql(SqlInterface $sqlInterface, string $name) : DatabaseAdapterInterface;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueries() : ?array;
	
	/**
	 * Return TRUE if list has been emptied and there are no more queries.
	 *
	 * @return bool
	 * @since   3.0.1 First time introduced.
	 */
	public function resetQueries() : bool;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult(string $name) : ?DatabaseResultInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriverName() : ?string;
	
	/**
	 * @return DatabaseAdapterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function createDriver() : DatabaseAdapterInterface;
}
