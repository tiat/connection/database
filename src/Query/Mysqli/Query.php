<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Query\Mysqli;

//
use mysqli as MysqliConnection;
use mysqli_result as MysqliResult;
use mysqli_sql_exception;
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Exception\DomainException;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\RuntimeException;
use Tiat\Connection\Database\Query\AbstractDatabaseQuery;
use Tiat\Connection\Database\Result\Mysqli\Result;

use function count;
use function get_debug_type;
use function is_array;
use function mysqli_real_escape_string;
use function sprintf;
use function str_ends_with;
use function substr;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Added support for asynchronous queries & multiple queries at once.
 */
class Query extends AbstractDatabaseQuery implements QueryInterface {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const int DEFAULT_RESULT_MODE = MYSQLI_STORE_RESULT;
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const int DEFAULT_USE_ASYNC_MODE = MYSQLI_ASYNC;
	
	/**
	 * @var int
	 * @since   3.1.0 First time introduced.
	 */
	private int $_resultMode = self::DEFAULT_RESULT_MODE;
	
	/**
	 * @var int
	 * @since   3.1.0 First time introduced.
	 */
	private int $_asyncMode = self::DEFAULT_RESULT_MODE;
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAsyncMode() : static {
		//
		$this->_asyncMode = self::DEFAULT_USE_ASYNC_MODE;
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed                $connection
	 * @param    null|array|string    $query
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Add support for multiple queries & async mode
	 */
	public function runQuery(DatabaseConnectionInterface $connection, array|string|null $query = NULL) : bool {
		// Check that there are queries
		if($this->hasQueries() === FALSE):
			return FALSE;
		endif;
		
		// Connection must be valid
		if($this->_checkConnection($connection)):
			// Get connection to inlined var
			if(( $c = $connection->getConnection() ) !== NULL):
				// Loop all queries
				// Add support for the Generator if possible
				foreach($this->_getQueryQueue() as $key => $val):
					// Create new Result object for each query
					// If not SELECT query then try to get last_insert_id & affected_rows for other queries
					try {
						// Check this always for several reasons to avoid problems
						if($c instanceof MysqliConnection):
							// If there are multiple queries, use mysqli_multi_query to execute multiple queries at once
							if(is_array($val) && count($val) >= 1):
								// Loop all queries in one string
								$queries  = '';
								$hashData = [];
								
								// Loop all queries in one string and execute multi_query
								foreach($val as $v):
									//
									if(str_ends_with($v, ';') === FALSE):
										$v .= ';';
									endif;
									
									// Generate hash for each query
									$hashData[] = $this->hashData(substr($v, 0, -1));
									
									// Add query to the string
									$queries .= ' ' . $v;
								endforeach;
								
								// Remove last semicolon from the string and execute multi_query
								$queries = substr($queries, 0, -1);
								
								// Check if multi_query was successful
								if($c->multi_query($queries) === TRUE):
									// Store results in Result objects and add them to the result array
									$r            = new Result((string)$key, $c, $hashData);
									$result[$key] = $r->setResult($c->store_result(), TRUE);
								else:
									throw new RuntimeException("Failed to execute multi_query.");
								endif;
							elseif(( $return = $c->query($val, $this->getAsyncMode() ?? $this->getResultMode()) )
							       instanceof MysqliResult):
								//
								$r            = new Result((string)$key, $c);
								$result[$key] = $r->setResult($return);
							elseif($return === TRUE):
								//
								$tmp = (object)[...['_connection' => $c], ...['insert_id' => $c->insert_id,
								                                              'affected_rows' => $c->affected_rows]];
								//
								$r            = new Result((string)$key, $tmp);
								$result[$key] = $r->setResult($tmp);
							elseif($return !== FALSE):
								$msg =
									sprintf("Return value from database is not TRUE (bool) or mysqli_result. Got %s.",
									        get_debug_type($return));
								throw new DomainException($msg);
							else:
								$msg = sprintf(" Got error %u with message %s.", $c->errno, $c->error);
								throw new RuntimeException($msg);
							endif;
						endif;
					} catch(RuntimeException $e) {
						throw new RuntimeException($e->getMessage());
					} catch(DomainException|mysqli_sql_exception $e) {
						throw new DomainException($e->getMessage() . ' at query: ' . serialize($val));
					}
				endforeach;
				
				//
				if(! empty($result)):
					$this->setQueryResult($result);
				endif;
				
				//
				return TRUE;
			else:
				throw new RuntimeException("We don't have connection to database server.");
			endif;
		else:
			$msg = sprintf("Connection type must be mysqli, got %s.", get_debug_type($connection->getConnection()));
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkConnection(DatabaseConnectionInterface $connection) : bool {
		return ( $connection->getConnection() instanceof MysqliConnection );
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _getQueryQueue() : ?array {
		//
		if(! is_array($this->getQueries())):
			return (array)$this->getQueries();
		endif;
		
		//
		return $this->getQueries();
	}
	
	/**
	 * @return int
	 * @since   3.1.0 First time introduced.
	 */
	public function getAsyncMode() : int {
		return $this->_asyncMode;
	}
	
	/**
	 * @param    int    $async
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setAsyncMode(int $async = self::DEFAULT_USE_ASYNC_MODE) : static {
		//
		$this->_asyncMode = $async;
		
		//
		return $this;
	}
	
	/**
	 * @return int
	 * @since 3.1.0 First time introduced.
	 */
	public function getResultMode() : int {
		return $this->_resultMode;
	}
	
	/**
	 * @param    int    $mode
	 *
	 * @return $this
	 * @since 3.1.0 First time introduced.
	 */
	public function setResultMode(int $mode) : static {
		//
		$this->_resultMode = $mode;
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed                               $value
	 * @param    null|DatabaseConnectionInterface    $connection
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function quoteValue(mixed $value, ?DatabaseConnectionInterface $connection = NULL) : string {
		//
		if($this->_checkConnection($connection = $connection ?? $this->_getConnection()) &&
		   ( $c = $connection?->getConnection() ) !== NULL):
			return mysqli_real_escape_string($c, $value);
		endif;
		
		//
		throw new RuntimeException("No database connection available.");
	}
}
