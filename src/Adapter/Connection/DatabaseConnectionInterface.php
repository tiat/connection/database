<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Connection;

//
use Jantia\Plugin\Monitor\Std\MonitorTimeInterface;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Exception\RuntimeException;
use Tiat\Connection\Database\Helper\SettingsInterface;
use Tiat\Connection\Stdlib\Connection\ConnectionInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DatabaseConnectionInterface extends ConnectionInterface, MonitorTimeInterface, SettingsInterface, ParametersPluginInterface {
	
	/**
	 * @param    DatabaseSettingsInterface    $settings
	 * @param    iterable                     $options
	 * @param                                 ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(DatabaseSettingsInterface $settings, iterable $options, ...$args);
	
	/**
	 * @param    DatabaseSettingsInterface    $settings
	 *
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettings(DatabaseSettingsInterface $settings) : DatabaseConnectionInterface;
	
	/**
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetSettings() : DatabaseConnectionInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : mixed;
	
	/**
	 * Prepare the database connection (check params etc.)
	 *
	 * @param    DatabaseSettingsInterface    $settings
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function prepareConnection(DatabaseSettingsInterface $settings) : bool;
	
	/**
	 * Driver array is same as the settings which are defined through __construct($params)
	 *
	 * @return DatabaseConnectionInterface
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnection() : DatabaseConnectionInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasConnection() : bool;
	
	/**
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function disconnect() : DatabaseConnectionInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getNamespace() : ?string;
	
	/**
	 * @param    string    $namespace
	 *
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setNamespace(string $namespace) : DatabaseConnectionInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionParamsHash() : ?string;
	
	/**
	 * @param    string    $hash
	 *
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionParamsHash(string $hash) : DatabaseConnectionInterface;
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isConnectionPrepared() : bool;
	
	/**
	 * @param    bool    $status
	 *
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionPrepared(bool $status) : DatabaseConnectionInterface;
	
	/**
	 * @param    array    $params
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function generateConnectionParamsHash(array $params) : string;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionParamsHashAlgo() : ?string;
	
	/**
	 * @param    string    $algo
	 *
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionParamsHashAlgo(string $algo) : DatabaseConnectionInterface;
	
	/**
	 * How long it took to connect to the database
	 *
	 * @return null|int
	 * @since   3.1.0 First time introduced.
	 */
	public function getConnectionTime() : ?int;
}
