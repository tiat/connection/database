<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter;

//
use Laminas\Db\Adapter\Adapter as LaminasAdapter;
use Laminas\Db\Sql\Sql as LaminasSql;
use Laminas\Db\Sql\SqlInterface;
use SplObjectStorage;
use Tiat\Collection\Crypt\CryptoTrait;
use Tiat\Collection\Crypt\CryptoTraitInterface;
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Adapter\Driver\DatabaseDriverInterface;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\RuntimeException;
use Tiat\Connection\Database\Helper\SettingsHelper;
use Tiat\Connection\Database\Query\DatabaseQueryInterface;
use Tiat\Connection\Database\Query\Mysqli\Query as MysqliQuery;
use Tiat\Connection\Database\Query\Postgres\Query as PostgresQuery;
use Tiat\Connection\Database\Result\DatabaseResultInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function count;
use function in_array;
use function is_array;
use function is_object;
use function register_shutdown_function;
use function sprintf;

/**
 * @version 3.0.1
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDatabaseAdapter implements CryptoTraitInterface, DatabaseAdapterInterface, ParametersPluginInterface {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	use CryptoTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use SettingsHelper;
	
	/**
	 * @var DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DatabaseDriverInterface $_driver;
	
	/**
	 * @var DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DatabaseSettingsInterface $_settings;
	
	/**
	 * @var DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	private DatabaseConnectionInterface $_connection;
	
	/**
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	private mixed $_connectionResult;
	
	/**
	 * @var SplObjectStorage
	 * @since   3.0.0 First time introduced.
	 */
	private SplObjectStorage $_query;
	
	/**
	 * @var SplObjectStorage
	 * @since   3.0.0 First time introduced.
	 */
	private SplObjectStorage $_result;
	
	/**
	 * @var LaminasAdapter
	 * @since   3.0.0 First time introduced.
	 */
	private LaminasAdapter $_queryAdapter;
	
	/**
	 * @var LaminasSql
	 * @since   3.0.0 First time introduced.
	 */
	private LaminasSql $_queryPlatform;
	
	/**
	 * @var array
	 * @since 3.1.0 First time introduced.
	 */
	private array $_queryDone;
	
	/**
	 * @var string
	 * @since 3.1.0 First time introduced.
	 */
	private string $_driverType;
	
	/**
	 * @param    null|DatabaseSettingsInterface    $settings
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(?DatabaseSettingsInterface $settings = NULL) {
		//
		register_shutdown_function([$this, 'shutdown']);
		
		//
		if($settings !== NULL):
			$this->setSettings($settings);
		endif;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	final public function resetSettings() : static {
		//
		unset($this->_settings);
		
		//
		return $this;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
	
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return AbstractDatabaseAdapter
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		//
		if(! empty($this->_query)):
			$this->_runQueryList();
		endif;
		
		//
		return $this;
	}
	
	/**
	 * This is deprecated from 3.1.0 and removed in 4.0.0
	 * This is replaced with Query Queue from Jantia\Database\Query\QueryQueue
	 *
	 * @return void
	 * @since      3.0.0 First time introduced.
	 * @deprecated 3.1.0 Use Query Queue from Jantia\Database\Query\QueryQueue
	 */
	protected function _runQueryList() : void {
		//
		if(( count($i = $this->_getQueryList() ?? []) ) > 0):
			// Go to first element
			$i->rewind();
			
			// Loop everything
			while($i->valid()):
				//
				$query = $i->current();
				
				// Generate hash from queries if those are done already
				if($query instanceof DatabaseQueryInterface):
					// Generate hash from ALL queries
					$hash = $this->hashData(serialize($query->getQueries()));
					
					// Check if query is already done
					// This will fix the issue with duplicate queries & queries that were run multiple times
					if(( $this->_isQueryDone($hash) === FALSE ) && $this->_getConnection() !== NULL):
						//
						$query->runQuery($this->_getConnection());
						$result = $query->getQueryResult($i->getInfo());
						
						//
						if($result instanceof DatabaseResultInterface):
							$this->_setResult($result);
						elseif(is_array($result)):
							foreach($result as $obj):
								if($obj instanceof DatabaseResultInterface):
									$this->_setResult($obj);
								endif;
							endforeach;
						endif;
						
						//
						$this->_mergeQueryDone($i->getInfo(), $hash);
					endif;
				endif;
				
				// Important
				$i->next();
			endwhile;
		endif;
	}
	
	/**
	 * Return all database queries en masse.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _getQueryList() : ?SplObjectStorage {
		return $this->_query ?? NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueries() : ?array {
		//
		if($this->_query->count() > 0):
			//
			$a = $this->_query;
			$a->rewind();
			
			// Loop everything
			while($a->valid()):
				// Get data from objects
				$obj = $a->current();
				if($obj instanceof DatabaseQueryInterface):
					$result = [...$result ?? [], ...$obj->getQueries()];
				endif;
				
				//
				$a->next();
			endwhile;
		endif;
		
		// Return result
		return $result ?? NULL;
	}
	
	/**
	 * @param    string    $hash
	 *
	 * @return bool
	 * @since  3.1.0 First time introduced.
	 */
	protected function _isQueryDone(string $hash) : bool {
		return in_array($hash, $this->_queryDone ?? [], TRUE);
	}
	
	/**
	 * @return null|DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnection() : ?DatabaseConnectionInterface {
		//
		if(empty($this->_connection)):
			$this->_setConnection();
		endif;
		
		//
		return $this->_connection ?? NULL;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConnection() : void {
		// Check if driver exists
		if(( $driver = $this->getDriver(TRUE) ) !== NULL && is_object($driver->getConnection())):
			$this->_connection = $driver->getConnection();
		endif;
	}
	
	/**
	 * @param    string    $name
	 * @param    string    $hash
	 *
	 * @return void
	 * @since  3.1.0 First time introduced.
	 */
	private function _mergeQueryDone(string $name, string $hash) : void {
		$this->_queryDone[$name] = $hash;
	}
	
	/**
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function getQueueInterface() : mixed {
		return NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.0.1 First time introduced.
	 */
	public function resetQueries() : bool {
		// Just clear the query list
		if(! empty($this->_query) && $this->_query->count() > 0):
			$this->_query = new SplObjectStorage();
		endif;
		
		//
		if(! empty($this->_query)):
			return ! (bool)$this->_query->count();
		else:
			return true;
		endif;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult(string $name) : ?DatabaseResultInterface {
		return $this->_searchResultByName($name);
	}
	
	/**
	 * @param    DatabaseResultInterface    $result
	 * @param    bool                       $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setResult(DatabaseResultInterface $result, bool $override = FALSE) : static {
		//
		if(empty($this->_result)):
			$this->_result = new SplObjectStorage();
		endif;
		
		//
		if($override === TRUE || $this->_result->offsetExists($result) === FALSE):
			$this->_result->offsetSet($result, $result->getName());
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	private function _searchResultByName(string $name) : ?DatabaseResultInterface {
		if(! empty($this->_result) && ( count($i = $this->_result) ) > 0):
			// Go to first element
			$i->rewind();
			
			// Loop everything
			while($i->valid()):
				//
				if($i->getInfo() === $name):
					return $i->current();
				endif;
				
				// Try until to end
				$i->next();
			endwhile;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriverName() : ?string {
		return $this->_getSettings()?->getDriver();
	}
	
	/**
	 * @param    bool    $autoCreate
	 *
	 * @return null|DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriver(bool $autoCreate = FALSE) : ?DatabaseDriverInterface {
		//
		if(empty($this->_driver) && $autoCreate === TRUE):
			$this->createDriver();
		endif;
		
		//
		return $this->_driver ?? NULL;
	}
	
	/**
	 * @param    DatabaseDriverInterface    $driver
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setDriver(DatabaseDriverInterface $driver) : static {
		//
		if(empty($this->_driver)):
			$this->_driver = $driver;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function createDriver() : static {
		//
		if(empty($this->_driver)):
			$this->setDriver($this->_createDriver($this->_getSettings()?->getDriver()));
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $driver
	 *
	 * @return DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _createDriver(string $driver) : DatabaseDriverInterface;
	
	/**
	 * @return null|DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _getSettings() : ?DatabaseSettingsInterface {
		return $this->_settings ?? NULL;
	}
	
	/**
	 * @param    DatabaseSettingsInterface    $settings
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	final public function setSettings(DatabaseSettingsInterface $settings) : static {
		//
		if(empty($this->_settings)):
			$this->_settings = $settings;
		else:
			throw new RuntimeException("Database settings has already been defined.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDriver() : static {
		//
		unset($this->_driver);
		
		//
		return $this;
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionResult() : mixed {
		return $this->_connectionResult ?? NULL;
	}
	
	/**
	 * @param    mixed    $result
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionResult(mixed $result) : static {
		//
		$this->_connectionResult = $result;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionResult() : static {
		//
		unset($this->_connectionResult);
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $queries
	 * @param    bool     $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueries(array $queries, bool $override = FALSE) : static {
		//
		foreach($queries as $name => $query):
			$this->setQuery($name, $query, $override);
		endforeach;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $list
	 * @param    bool     $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryInterfaces(array $list, bool $override = FALSE) : static {
		//
		foreach($list as $query):
			if($query instanceof DatabaseQueryInterface):
				$this->setQueryInterface($query, $query->getName(), $override);
			endif;
		endforeach;
		
		//
		return $this;
	}
	
	/**
	 * @param    DatabaseQueryInterface    $query
	 * @param    null|string|int           $name
	 * @param    bool                      $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryInterface(DatabaseQueryInterface $query, string|int|null $name = NULL, bool $override = FALSE) : static {
		//
		if(empty($this->_query)):
			$this->_query = new SplObjectStorage();
		endif;
		
		//
		if($override === TRUE || $this->_query->offsetExists($query) === FALSE):
			$this->_query->offsetSet($query, $name ?? $query->getName());
		else:
			$msg = sprintf("Query with name %s already exists and override is denied.", $name ?? $query->getName());
			throw new RuntimeException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    SqlInterface    $sqlInterface
	 * @param    string          $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function addPlatformSql(SqlInterface $sqlInterface, string $name) : static {
		//
		if(( $i = $this->getQuery($name) ) !== NULL):
			$this->setQueryInterface($i->addQuery($name,
			                                      (string)$this->getQueryPlatform()?->buildSqlString($sqlInterface)));
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|int    $name
	 *
	 * @return null|DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery(string|int $name) : ?DatabaseQueryInterface {
		// Get query interface
		if(! empty($driver = $this->getDriverType())):
			return match ( $driver ) {
				self::DRIVER_MARIADB, self::DRIVER_MYSQLI, self::DRIVER_MYSQL => new MysqliQuery($name,
				                                                                                 $this->_getConnection()),
				self::DRIVER_POSTGRES => new PostgresQuery($name, $this->_getConnection()),
				self::DRIVER_PDO => throw new RuntimeException(sprintf("Driver %s implementation is still going on.",
				                                                       $driver)),
				default => throw new InvalidArgumentException(sprintf("Given driver (%s) hasn't right query interface in this library.",
				                                                      $driver))
			};
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string|int      $name
	 * @param    array|string    $query
	 * @param    bool            $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setQuery(string|int $name, array|string $query, bool $override = FALSE) : static {
		//
		if(( $q = $this->getQuery($name) ) !== NULL):
			$this->_setQuery($q, $query);
		endif;
		
		//
		return $this->setQueryInterface($q, override:$override);
	}
	
	/**
	 * @param    DatabaseQueryInterface    $q
	 * @param    array|string              $query
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setQuery(DatabaseQueryInterface $q, array|string $query) : void {
		// Set query
		if(is_array($query)):
			foreach($query as $value):
				$this->_setQuery($q, $value);
			endforeach;
		else:
			$q->addQuery($q->getName(), $query);
		endif;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriverType() : ?string {
		//
		if(empty($this->_driverType)):
			$this->_setDriverType();
		endif;
		
		//
		return $this->_driverType ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setDriverType() : static {
		//
		if(empty($this->_driverType)):
			$this->_driverType = $this->_getSettings()?->getDriver();
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    bool    $new
	 *
	 * @return null|LaminasSql
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryPlatform(bool $new = FALSE) : ?LaminasSql {
		// Get query platform
		if(empty($this->_queryPlatform) || $new === TRUE):
			if(( $adapter = $this->getQueryAdapter() ) !== NULL):
				$this->_queryPlatform = new LaminasSql($adapter);
			endif;
		endif;
		
		//
		return $this->_queryPlatform ?? NULL;
	}
	
	/**
	 * @param    bool    $new
	 *
	 * @return null|LaminasAdapter
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryAdapter(bool $new = FALSE) : ?LaminasAdapter {
		// Get query adapter
		if(empty($this->_queryAdapter) || $new === TRUE):
			if(( $e = new LaminasAdapter($this->_getSettings()?->getConfigArray()) )):
				$this->_queryAdapter = $e;
			endif;
		endif;
		
		//
		return $this->_queryAdapter ?? NULL;
	}
}
