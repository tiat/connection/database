<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Query\Postgres;

//
use PgSql\Connection as PgSqlConnection;
use PgSql\Result as PgSqlResult;
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\RuntimeException;
use Tiat\Connection\Database\Query\AbstractDatabaseQuery;
use Tiat\Connection\Database\Result\Postgres\Result;

use function get_debug_type;
use function is_array;
use function pg_connection_status;
use function pg_escape_string;
use function pg_last_error;
use function pg_ping;
use function pg_query;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Query extends AbstractDatabaseQuery implements QueryInterface {
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 * @param    null|array|string              $query
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function runQuery(DatabaseConnectionInterface $connection, array|string|null $query = NULL) : bool {
		// Check that there are queries
		if($query === NULL && empty($query = $this->getQueries())):
			return FALSE;
		endif;
		
		// Connection must be valid
		if($this->_checkConnection($connection)):
			//
			if(! is_array($query)):
				$query = [$query];
			endif;
			
			// Get connection to inlined var
			if(( $c = $connection->getConnection() ) !== NULL):
				// Refresh connection if it's not open
				if(pg_connection_status($c) !== PGSQL_CONNECTION_OK && pg_ping($c) === FALSE):
					//
					$msg = "Database connection doesn't exists.";
					throw new RuntimeException($msg);
				endif;
				
				//
				foreach($query as $key => $val):
					try {
						// Check this always for several reasons to avoid problems
						if($c instanceof PgSqlConnection):
							// Result is ok
							if(( $return = pg_query($c, $val) ) instanceof PgSqlResult):
								$r            = new Result((string)$key, ( $c ));
								$result[$key] = $r->setResult($return);
							else:
								// Result failed
								$msg = sprintf(" Got error with message %s.", pg_last_error($c));
								throw new RuntimeException($msg);
							endif;
						endif;
					} catch(RuntimeException $e) {
						throw new RuntimeException($e->getMessage());
					}
				endforeach;
				
				//
				if(! empty($result)):
					$this->setQueryResult($result);
				endif;
				
				//
				return TRUE;
			else:
				throw new RuntimeException("We don't have connection to database server.");
			endif;
		else:
			$msg = sprintf("Connection type must be PgSql\Connection, got %s.",
			               get_debug_type($connection->getConnection()));
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkConnection(DatabaseConnectionInterface $connection) : bool {
		return ( $connection->getConnection() instanceof PgSqlConnection );
	}
	
	/**
	 * @param    mixed                               $value
	 * @param    null|DatabaseConnectionInterface    $connection
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function quoteValue(mixed $value, ?DatabaseConnectionInterface $connection = NULL) : string {
		//
		if($this->_checkConnection($connection = $connection ?? $this->_getConnection()) &&
		   ( $c = $connection?->getConnection() ) !== NULL):
			return '\'' . pg_escape_string($c, $value) . '\'';
		endif;
		
		//
		throw new RuntimeException("No database connection available.");
	}
}
