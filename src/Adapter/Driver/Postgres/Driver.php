<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Driver\Postgres;

//
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Adapter\Connection\Postgres\Connection;
use Tiat\Connection\Database\Adapter\Driver\AbstractDatabaseDriver;
use Tiat\Connection\Database\Exception\RuntimeException;

use function extension_loaded;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Driver extends AbstractDatabaseDriver {
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(DatabaseConnectionInterface $connection = new Connection()) {
		parent::__construct($connection);
	}
	
	/**
	 * @return bool
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function checkExtension() : bool {
		//
		if(! extension_loaded('pgsql')):
			throw new RuntimeException("Pgsql extension is required but the extension is not loaded.");
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @param    string    $name
	 * @param              $type
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function formatParameterName(string $name, $type = NULL) : string {
		return '$#';
	}
}
