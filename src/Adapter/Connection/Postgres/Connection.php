<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Connection\Postgres;

//
use Exception;
use PgSql\Connection as PgSqlConnection;
use Tiat\Collection\Uri\Ip;
use Tiat\Connection\Database\Adapter\Connection\AbstractDatabaseConnection;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Adapter\Settings\Postgres\DatabaseSettingsPostgres;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\OutOfBoundsException;
use Tiat\Connection\Database\Exception\RuntimeException;

use function implode;
use function pg_close;
use function pg_connect;
use function pg_last_error;
use function sprintf;
use function strtoupper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Connection extends AbstractDatabaseConnection {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DATABASE_CHARSET = 'utf8';
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_connectAsync = FALSE;
	
	/**
	 * Use new connection to DB server as default.
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_connectForceNew = TRUE;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_connectionString;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_connectionFlags;
	
	/**
	 * @var null|array
	 * @since   3.0.0 First time introduced.
	 */
	private ?array $_connectionStringOptions;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_options;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : mixed {
		//
		if(empty($this->_connection) || ! $this->_connection instanceof PgSqlConnection):
			$this->setConnection();
		endif;
		
		//
		return $this->_connection ?? NULL;
	}
	
	/**
	 * @param    array    $options
	 * @param    bool     $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionStringOptions(array $options, bool $override = FALSE) : static {
		//
		if(! empty($options)):
			foreach($options as $key => $val):
				$this->setConnectionStringOption($key, $val, $override);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 * @param    mixed     $value
	 * @param    bool      $override
	 *
	 * @return $this
	 * @throws OutOfBoundsException
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionStringOption(string $name, mixed $value, bool $override = FALSE) : static {
		// Check if key exists and override is allowed
		if(! isset($this->_connectionStringOptions[$name]) || $override === TRUE):
			$this->_connectionStringOptions[$name] = $value;
		else:
			$msg = sprintf("Key '%s' already exists and can't be overridden.", $name);
			throw new OutOfBoundsException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    int     $flag
	 * @param    bool    $value
	 *
	 * @return void
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionFlag(int $flag, bool $value) : void {
		// Check if flag is valid
		match ( $flag ) {
			PGSQL_CONNECT_ASYNC => $this->_connectAsync = $value,
			PGSQL_CONNECT_FORCE_NEW => $this->_connectForceNew = $value,
			default => throw new InvalidArgumentException(sprintf("Given flag '%u' is not supported.", $flag))
		};
	}
	
	/**
	 * @return $this
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function disconnect() : static {
		if(! empty($this->_connection) && $this->_connection instanceof PgSqlConnection):
			//
			try {
				pg_close($this->_connection);
			} catch(RuntimeException $e) {
				echo $e->getMessage();
			}
			
			// Remove the object anyway
			$this->_connection = NULL;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConnection() : static {
		//
		if(empty($this->_connection)):
			try {
				if(( $conn = $this->_getConnectionString() ) !== NULL):
					// Start monitoring the time for connection
					$this->setMonitoringTime(self::MONITORING_START);
					
					// Connect to database
					if(( $e = pg_connect($conn, $this->getConnectionFlags()) ) !== FALSE &&
					   $e instanceof PgSqlConnection):
						$this->_connection = $e;
					else:
						throw new RuntimeException(pg_last_error($e));
					endif;
					
					// Stop monitoring the time for connection
					$this->setMonitoringTime(self::MONITORING_STOP);
					
					// Save connection details for debugging
					$this->_setConnectionTime(self::getMonitoringTime());
				else:
					throw new RuntimeException("Can't create connection string for database connection.");
				endif;
			} catch(Exception $e) {
				throw new RuntimeException($e->getMessage());
			}
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getConnectionString() : ?string {
		//
		if(empty($this->_connectionString)):
			$this->_setConnectionString();
		endif;
		
		return $this->_connectionString ?? NULL;
	}
	
	/**
	 * @return Connection
	 * @since   3.0.0 First time introduced.
	 */
	private function _setConnectionString() : static {
		//
		if(empty($this->_connectionString) &&
		   ( $settings = $this->_getSettings() ) instanceof DatabaseSettingsPostgres):
			//
			if(( ! empty($host = $settings->getHostname()) ) && ( new Ip() )->isValid($host) === FALSE):
				$array['host'] = $host;
			else:
				$array['hostaddr'] = $host;
			endif;
			
			//
			$array['port']            = $settings->getPort();
			$array['dbname']          = $settings->getDatabase();
			$array['user']            = $settings->getUsername();
			$array['password']        = $settings->getPassword();
			$array['connect_timeout'] = $settings->getTimeout();
			$array['options']         = $this->_getConnectionStringOptions();
			
			//
			$result = [];
			foreach($array as $key => $val):
				if(! empty($val)):
					$result[] = $key . '=' . $val;
				endif;
			endforeach;
			
			//
			if(! empty($result)):
				// This line will run this method only once & other calls will get result from internal cache
				$this->_connectionString = implode(' ', $result);
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return DatabaseSettingsInterface|DatabaseSettingsPostgres
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _getSettings() : DatabaseSettingsInterface|DatabaseSettingsPostgres {
		return parent::_getSettings();
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getConnectionStringOptions() : ?string {
		if(empty($this->_options)):
			$this->_setConnectionStringOptions();
		endif;
		
		//
		return $this->_options ?? NULL;
	}
	
	/**
	 * @return Connection
	 * @since   3.0.0 First time introduced.
	 */
	private function _setConnectionStringOptions() : static {
		// Get client encoding
		if(empty($this->_options)):
			// Get the database charset
			$array['--client_encoding'] = strtoupper($this->_getSettings()->getCharset());
			
			// Other options like application name etc
			if(! empty($options = $this->getConnectionOptions())):
				foreach($options as $key => $val):
					$array['--' . $key] = $val;
				endforeach;
			endif;
			
			//
			$result = [];
			foreach($array as $key => $val):
				if(! empty($val)):
					$result[] = $key . '=' . $val;
				endif;
			endforeach;
			
			//
			if(! empty($result)):
				$this->_options = "'" . implode(' ', $result) . "'";
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionFlags() : int {
		//
		if(empty($this->_connectionFlags)):
			$this->setConnectionFlags();
		endif;
		
		//
		return $this->_connectionFlags ?? 0;
	}
	
	/**
	 * @return Connection
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionFlags() : static {
		// This must be at first
		if(empty($this->_connectionFlags)):
			// This is default
			if($this->_connectForceNew === TRUE):
				$this->_connectionFlags = PGSQL_CONNECT_FORCE_NEW;
			endif;
			
			//
			if($this->_connectAsync === TRUE):
				$this->_connectionFlags = PGSQL_CONNECT_ASYNC;
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionParams() : array {
		return [$this->_getConnectionString(), $this->getConnectionFlags()];
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionParamValues() : array {
		return [...parent::_getConnectionParamValues(), ...['timeout', 'port', 'socket']];
	}
}
