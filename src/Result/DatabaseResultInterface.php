<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Result;

//
use Countable;
use Iterator;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DatabaseResultInterface extends Countable, Iterator {
	
	/**
	 * Set result name
	 *
	 * @param    string    $name
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : DatabaseResultInterface;
	
	/**
	 * Get result name.
	 * When you have multiple queries on same object then you can get the right result with name.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string;
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function count() : int;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function current() : mixed;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function key() : mixed;
	
	/**
	 * Move pointer to next item
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function next() : void;
	
	/**
	 * Move pointer to previous item
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function rewind() : void;
	
	/**
	 * Get data by row number
	 *
	 * @param    int    $number
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function row(int $number) : mixed;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function valid() : bool;
	
	/**
	 * @param ...$args
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : DatabaseResultInterface;
	
	/**
	 * @param ...$args
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : bool;
	
	/**
	 * @param    bool    $buffered
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setBuffered(bool $buffered) : void;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isBuffered() : bool;
	
	/**
	 * @param    bool    $buffered
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setBufferedCompleted(bool $buffered) : void;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isBufferedCompleted() : bool;
	
	/**
	 * @param    mixed    $data
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setBufferedData(mixed $data) : DatabaseResultInterface;
	
	/**
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetBufferedData() : DatabaseResultInterface;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getFieldInfo() : ?array;
	
	/**
	 * @param    array    $fields
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setFieldInfo(array $fields) : DatabaseResultInterface;
	
	/**
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetFieldInfo() : DatabaseResultInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getBufferedData() : mixed;
	
	/**
	 * @param    bool    $status
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCurrentAllDataCompleted(bool $status) : DatabaseResultInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurrentAllDataCompleted() : bool;
	
	/**
	 * @param    bool    $status
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCurrentCompleted(bool $status) : DatabaseResultInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurrentCompleted() : bool;
	
	/**
	 * @param    array    $data
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCurrentData(array $data) : DatabaseResultInterface;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurrentData() : ?array;
	
	/**
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCurrentData() : DatabaseResultInterface;
	
	/**
	 * @param    mixed    $result
	 *
	 * @return DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setResult(mixed $result) : DatabaseResultInterface;
	
	/**
	 * Is query result
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isResult() : bool;
	
	/**
	 * Return rows affected (update/delete/insert). Return NULL in failure
	 * Notice! Postgres will return affected rows also with SELECT with Postgres 9.0 and above
	 *
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getAffectedRows() : ?int;
	
	/**
	 * Last inserted row ID
	 *
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function lastInserted() : ?int;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : mixed;
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getResultMode() : ?int;
	
	/**
	 * @param    int    $mode
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setResultMode(int $mode) : void;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void;
	
	/**
	 * Get all rows as array
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function toArray() : ?array;
}
