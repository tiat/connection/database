<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Driver;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DatabaseDriverInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	const string PARAMETERIZATION_POSITIONAL = 'positional';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	const string PARAMETERIZATION_NAMED = 'named';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	const string NAME_FORMAT_CAMELCASE = 'camelCase';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	const string NAME_FORMAT_NATURAL = 'natural';
	
	/**
	 * Set database driver basename (mysqli, pgsql etc)
	 *
	 * @param    string    $name
	 *
	 * @return DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : DatabaseDriverInterface;
	
	/**
	 * Get database driver basename (mysqli, pgsql etc)
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : mixed;
	
	/**
	 * Check that the driver extension is loaded.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkExtension() : bool;
	
	/**
	 * @param ...$args
	 *
	 * @return DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : DatabaseDriverInterface;
	
	/**
	 * @param ...$args
	 *
	 * @return DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : DatabaseDriverInterface;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void;
	
	/**
	 * @param    string    $name
	 * @param              $type
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function formatParameterName(string $name, $type = NULL) : string;
}
