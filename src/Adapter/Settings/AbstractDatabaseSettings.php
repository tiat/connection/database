<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Settings;

//
use Laminas\Stdlib\ArrayUtils;
use Tiat\Collection\Uri\Ip;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Helper\SettingsHelper;
use Tiat\Connection\Database\Helper\SettingsInterface;
use Tiat\Connection\Stdlib\Adapter\ConnectionParamKeywords;

use function get_defined_vars;
use function gethostbyname;
use function in_array;
use function is_string;
use function key;
use function method_exists;
use function sprintf;
use function strtolower;
use function trim;
use function ucfirst;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDatabaseSettings implements DatabaseSettingsInterface, SettingsInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use SettingsHelper;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_settingDatabase;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_settingHostname;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_settingPassword;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_settingUsername;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_settingsDriver;
	
	/**
	 * @param    iterable         $config
	 * @param    null|iterable    $options
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $config, ?iterable $options = NULL) {
		//
		$this->setConfig($config);
		
		//
		$this->setConnectionOptions($options);
	}
	
	/**
	 * @param    iterable    $config
	 *
	 * @return AbstractDatabaseSettings
	 * @since   3.0.0 First time introduced.
	 */
	public function setConfig(iterable $config) : static {
		if(! empty($config)):
			// Convert array to associative array for easier handling
			$array = ArrayUtils::iteratorToArray((array)$config);
			
			// Set settings from array
			$accept = $this->getConnectionParamValues();
			foreach($array as $name => $val):
				if(in_array($name, $accept, TRUE)):
					$this->checkSettingsValue($name, $val);
				endif;
			endforeach;
			
			// Get driver name from settings
			$this->setDriver($array[ConnectionParamKeywords::DRIVER->value]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return string[]
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionParamValues() : array {
		return ['hostname', 'username', 'password', 'database'];
	}
	
	/**
	 * @param    string    $name
	 * @param    mixed     $value
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function checkSettingsValue(string $name, mixed $value) : static {
		// Convert value to lowercase for consistency
		if(is_string($value = trim($value)) && ! empty($value)):
			match ( $name ) {
				'database' => $this->_settingDatabase = $value,
				'hostname' => $this->_settingHostname = $value,
				'password' => $this->_settingPassword = $value,
				'username' => $this->_settingUsername = $value,
				default => throw new InvalidArgumentException(sprintf("Given name '%s' has no handler.", $name))
			};
		else:
			$msg = sprintf("%s is empty or it's not valid.", $name);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $driver
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setDriver(string $driver) : static {
		//
		$this->_settingsDriver = $driver;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfigArray() : ?array {
		//
		if(! empty($values = $this->getConnectionParamValues())):
			// Create an associative array with setter method names as keys and values as empty strings
			foreach($values as $val):
				if(method_exists($this, $method = $this->_getSetterName($val))):
					$result[$val] = $this->$method();
				endif;
			endforeach;
			
			// Add driver to the array if it's not empty
			if(! empty($result)):
				$result = [...['driver' => $this->convertDriverName($this->getDriver())], ...$result];
			endif;
		endif;
		
		//
		return $result ?? [];
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getSetterName(string $name) : string {
		return 'get' . ( ucfirst(strtolower($name)) );
	}
	
	/**
	 * @param    string    $driver
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function convertDriverName(string $driver) : string {
		return match ( $driver ) {
			self::DRIVER_MARIADB, self::DRIVER_MYSQLI, self::DRIVER_MYSQL => self::DRIVER_MYSQLI,
			self::DRIVER_POSTGRES => 'pgsql',
			default => self::DRIVER_PDO
		};
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriver() : ?string {
		return $this->_settingsDriver ?? NULL;
	}
	
	/**
	 * @param    string    $database
	 *
	 * @return AbstractDatabaseSettings
	 * @since   3.0.0 First time introduced.
	 */
	public function setDatabase(string $database) : static {
		return $this->checkSettingsValue(key(get_defined_vars()), $database);
	}
	
	/**
	 * @param    null|string    $hostname
	 * @param    bool           $convert
	 *
	 * @return AbstractDatabaseSettings
	 * @since   3.0.0 First time introduced.
	 */
	public function setHostname(?string $hostname = NULL, bool $convert = FALSE) : static {
		return $this->checkSettingsValue(key(get_defined_vars()), $hostname)->_setHostname($convert);
	}
	
	/**
	 * Convert DNS hostname to IP address
	 *
	 * @param    bool    $convert
	 *
	 * @return AbstractDatabaseSettings
	 * @since   3.0.0 First time introduced.
	 */
	private function _setHostname(bool $convert = FALSE) : static {
		// Convert DNS hostname to IP address if needed
		if($convert === TRUE && ! empty($name = trim($this->getHostname()))):
			//
			$net = new Ip();
			
			// Already an IP
			if($net->isValid($name, TRUE) === TRUE):
				return $this;
			elseif(( $ip = gethostbyname($name) ) !== $name && $net->isValid($ip, TRUE) === TRUE):
				// Hostname has been converted to a valid IP
				$this->setHostname($ip);
			elseif(strtolower($ip) === strtolower($name)):
				// Example on Docker environment gethostbyname may return a name (not an IP)
				// Keep the value but this can lead the problem when trying to connect database if value is not a valid network hostname
				return $this;
			else:
				// Is this even possible?
				$msg = sprintf("Hostname value (%s') is not valid.", $name);
				throw new InvalidArgumentException($msg);
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHostname() : ?string {
		return $this->_settingHostname ?? NULL;
	}
	
	/**
	 * @param    string    $password
	 *
	 * @return AbstractDatabaseSettings
	 * @since   3.0.0 First time introduced.
	 */
	public function setPassword(string $password) : static {
		return $this->checkSettingsValue(key(get_defined_vars()), $password);
	}
	
	/**
	 * @param    string    $username
	 *
	 * @return AbstractDatabaseSettings
	 * @since   3.0.0 First time introduced.
	 */
	public function setUsername(string $username) : static {
		return $this->checkSettingsValue(key(get_defined_vars()), $username);
	}
	
	/**
	 * Remove all connection param values
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionParams() : void {
		unset($this->_settingDatabase, $this->_settingHostname, $this->_settingUsername, $this->_settingPassword);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUsername() : ?string {
		return $this->_settingUsername ?? NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getPassword() : ?string {
		return $this->_settingPassword ?? NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDatabase() : ?string {
		return $this->_settingDatabase ?? NULL;
	}
}
