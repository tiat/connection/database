<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Result\Postgres;

//
use PgSql\Connection as PgSqlConnection;
use PgSql\Result as PgSqlResult;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\RuntimeException;
use Tiat\Connection\Database\Result\AbstractDatabaseResult;

use function get_debug_type;
use function pg_affected_rows;
use function pg_fetch_all;
use function pg_free_result;
use function pg_num_fields;
use function pg_num_rows;
use function pg_result_seek;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Result extends AbstractDatabaseResult {
	
	/**
	 * @var PgSqlResult
	 * @since   3.0.0 First time introduced.
	 */
	private PgSqlResult $_result;
	
	/**
	 * @param    string             $name
	 * @param    PgSqlConnection    $_connection
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string $name, private readonly PgSqlConnection $_connection) {
		//
		parent::__construct($name);
		
		//
		$this->setResultMode(PGSQL_ASSOC);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
		//
		if(( $e = $this->getResult() ) !== NULL):
			pg_free_result($e);
		endif;
		
		//
		$this->resetBufferedData();
		$this->resetCurrentData();
	}
	
	/**
	 * @return null|PgSqlResult
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : ?PgSqlResult {
		return $this->_result ?? NULL;
	}
	
	/**
	 * Set $result as instance of PgSql\Result
	 *
	 * @param    mixed    $result
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setResult(mixed $result) : static {
		//
		if($result instanceof PgSqlResult):
			$this->_result = $result;
		else:
			$msg = sprintf("Variable must be instance of PgSql\Result, got '%s'.", get_debug_type($result));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function isResult() : bool {
		if(( $e = $this->getResult() ) !== NULL):
			return ( pg_num_fields($e) > 0 );
		endif;
		
		//
		throw new RuntimeException("No database result available.");
	}
	
	/**
	 * @return int
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function count() : int {
		if(( $e = $this->getResult() ) !== NULL):
			return pg_num_rows($e);
		endif;
		
		//
		throw new RuntimeException("No database result available.");
	}
	
	/**
	 * @return null|int
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function getAffectedRows() : ?int {
		if(( $e = $this->getResult() ) !== NULL):
			return pg_affected_rows($e);
		endif;
		
		//
		throw new RuntimeException("No database result available.");
	}
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function lastInserted() : ?int {
		return $this->_connection->insert_id ?? NULL;
	}
	
	/**
	 * @return array
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	protected function _fetchAllData() : array {
		try {
			if(( $e = $this->getResult() ) !== NULL):
				return pg_fetch_all($e, $this->getResultMode());
			else:
				throw new RuntimeException("No database result available.");
			endif;
		} catch(RuntimeException $e) {
			throw new RuntimeException($e->getMessage());
		}
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _validResultMode() : array {
		return [PGSQL_ASSOC, PGSQL_NUM, PGSQL_BOTH];
	}
	
	/**
	 * @return array
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	protected function _fetchNextRow() : array {
		//
		if(( $e = $this->getResult() ) !== NULL):
			return (array)pg_fetch_assoc($e);
		endif;
		
		//
		throw new RuntimeException("No database result available.");
	}
	
	/**
	 * @param    int    $row
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setRow(int $row) : bool {
		// Move data pointer
		if($this->isBuffered() === FALSE && ( $e = $this->getResult() ) !== NULL && pg_result_seek($e, $row) === TRUE):
			//
			$this->_setRowReset();
			
			//
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
}
