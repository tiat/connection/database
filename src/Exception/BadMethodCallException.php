<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface {

}
