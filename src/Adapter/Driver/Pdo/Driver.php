<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Driver\Pdo;

//
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Adapter\Connection\Pdo\Connection;
use Tiat\Connection\Database\Adapter\Driver\AbstractDatabaseDriver;
use Tiat\Connection\Database\Exception\RuntimeException;

use function extension_loaded;
use function is_numeric;
use function ltrim;
use function preg_match;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Driver extends AbstractDatabaseDriver {
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(DatabaseConnectionInterface $connection = new Connection()) {
		parent::__construct($connection);
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkExtension() : bool {
		//
		if(! extension_loaded('pdo_sqlite')):
			throw new RuntimeException("Mysqlnd extension is required but the extension is not loaded.");
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @param    string    $name
	 * @param              $type
	 *
	 * @return string
	 * @see     https://bugs.php.net/bug.php?id=43130
	 * @since   3.0.0 First time introduced.
	 */
	public function formatParameterName(string $name, $type = NULL) : string {
		//
		if(( $type === NULL && ! is_numeric($name) ) || $type === self::PARAMETERIZATION_NAMED):
			//
			$name = ltrim($name, ':');
			
			//
			if(preg_match('/\W/', $name)):
				throw new RuntimeException(sprintf('The PDO param %s contains invalid characters.' .
				                                   ' Only alphabetic characters, digits, and underscores (_)' .
				                                   ' are allowed.', $name));
			endif;
			
			//
			return ':' . $name;
		endif;
		
		//
		return '?';
	}
}
