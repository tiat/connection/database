<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Query\Mysqli;

//
use Tiat\Connection\Database\Query\DatabaseQueryInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface QueryInterface extends DatabaseQueryInterface {
	
	/**
	 * @return int
	 * @since 3.1.0 First time introduced.
	 */
	public function getResultMode() : int;
	
	/**
	 * @param    int    $mode
	 *
	 * @return QueryInterface
	 * @since 3.1.0 First time introduced.
	 */
	public function setResultMode(int $mode) : QueryInterface;
	
	/**
	 * @return int
	 * @since   3.1.0 First time introduced.
	 */
	public function getAsyncMode() : int;
	
	/**
	 * @param    int    $async
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setAsyncMode(int $async) : QueryInterface;
	
	/**
	 * @return QueryInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAsyncMode() : QueryInterface;
}
