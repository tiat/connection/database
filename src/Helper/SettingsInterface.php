<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Helper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface SettingsInterface {
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionOptions() : ?array;
	
	/**
	 * @param    array    $options
	 *
	 * @return SettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionOptions(array $options) : SettingsInterface;
	
	/**
	 * Get default connection options
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionOptionsDefault() : array;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isInitCompleted() : bool;
	
	/**
	 * @param    bool    $status
	 *
	 * @return SettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setInitCompleted(bool $status) : SettingsInterface;
}
