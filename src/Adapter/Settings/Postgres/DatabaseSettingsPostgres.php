<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Settings\Postgres;

//
use Tiat\Connection\Database\Adapter\Connection\Postgres\Connection;
use Tiat\Connection\Database\Adapter\Settings\AbstractDatabaseSettings;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Exception\InvalidArgumentException;

use function get_defined_vars;
use function is_numeric;
use function is_string;
use function key;
use function sprintf;
use function trim;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DatabaseSettingsPostgres extends AbstractDatabaseSettings implements DatabaseSettingsPostgresInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int DEFAULT_SETTINGS_PORT = 5432;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int DEFAULT_SETTINGS_TIMEOUT = 5;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_settingsCharset = Connection::DATABASE_CHARSET;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_settingsPort = self::DEFAULT_SETTINGS_PORT;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_settingsTimeout = self::DEFAULT_SETTINGS_TIMEOUT;
	
	/**
	 * @var null|string
	 * @since   3.0.0 First time introduced.
	 */
	private ?string $_settingsSocket;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCharset() : string {
		return $this->_settingsCharset;
	}
	
	/**
	 * @param    string    $charset
	 *
	 * @return DatabaseSettingsPostgres
	 * @since   3.0.0 First time introduced.
	 */
	public function setCharset(string $charset) : static {
		return $this->checkSettingsValue(key(get_defined_vars()), $charset);
	}
	
	/**
	 * @param    string    $name
	 * @param    mixed     $value
	 *
	 * @return $this
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function checkSettingsValue(string $name, mixed $value) : static {
		//
		if($name === 'port' && is_numeric($value)):
			$this->_settingsPort = (int)$value;
		elseif($name === 'timeout' && is_numeric($value)):
			$this->_settingsTimeout = (int)$value;
		elseif(is_string($value = trim($value)) && ! empty($value)):
			match ( $name ) {
				'charset' => $this->_settingsCharset = $value,
				default => parent::checkSettingsValue($name, $value)
			};
		else:
			$msg = sprintf("%s is empty or it's not valid.", $name);
			throw new InvalidArgumentException($msg, E_USER_ERROR);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	final public function getConnectionParamValues() : array {
		return [...parent::getConnectionParamValues(), ...['port', 'charset', 'timeout']];
	}
	
	/**
	 * @param    null|string    $socket
	 *
	 * @return DatabaseSettingsPostgres
	 * @since   3.0.0 First time introduced.
	 */
	public function setSocket(?string $socket) : static {
		return $this->checkSettingsValue(key(get_defined_vars()), $socket);
	}
	
	/**
	 * @param    int    $port
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPort(int $port) : DatabaseSettingsInterface {
		return $this->checkSettingsValue(key(get_defined_vars()), $port);
	}
	
	/**
	 * @param    int    $timeout
	 *
	 * @return DatabaseSettingsPostgres
	 * @since   3.0.0 First time introduced.
	 */
	public function setTimeout(int $timeout) : static {
		return $this->checkSettingsValue(key(get_defined_vars()), $timeout);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionParams() : void {
		//
		$this->_settingsCharset = Connection::DATABASE_CHARSET;
		$this->_settingsPort    = self::DEFAULT_SETTINGS_PORT;
		$this->_settingsTimeout = self::DEFAULT_SETTINGS_TIMEOUT;
		
		//
		unset($this->_settingsSocket);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSocket() : ?string {
		return $this->_settingsSocket ?? NULL;
	}
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getPort() : int {
		return $this->_settingsPort;
	}
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getTimeout() : int {
		return $this->_settingsTimeout;
	}
}
