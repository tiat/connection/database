<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter;

/**
 * @version 3.0.1
 * @since   3.0.1 First time introduced.
 */
interface DatabaseInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DRIVER_MARIADB = 'mariadb';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DRIVER_MYSQLI = 'mysqli';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DRIVER_MYSQL = 'mysql';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DRIVER_POSTGRES = 'postgres';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DRIVER_PDO = 'pdo';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DRIVER_SQLITE = 'sqlite';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DRIVER_SQL92 = 'sql92';
	
	/**
	 * @since  3.1.0 First time introduced.
	 */
	public const array DRIVERS_MYSQL_BASE = [self::DRIVER_MYSQL, self::DRIVER_MYSQLI, self::DRIVER_MARIADB];
}