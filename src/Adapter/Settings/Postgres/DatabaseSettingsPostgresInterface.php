<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Settings\Postgres;

//
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DatabaseSettingsPostgresInterface extends DatabaseSettingsInterface {
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getTimeout() : int;
	
	/**
	 * @param    int    $timeout
	 *
	 * @return DatabaseSettingsPostgresInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setTimeout(int $timeout) : DatabaseSettingsPostgresInterface;
	
	/**
	 * @param    null|string    $socket
	 *
	 * @return DatabaseSettingsPostgresInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSocket(?string $socket) : DatabaseSettingsPostgresInterface;
}
