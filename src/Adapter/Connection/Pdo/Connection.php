<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Connection\Pdo;

//
use Tiat\Connection\Database\Adapter\Connection\AbstractDatabaseConnection;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Connection extends AbstractDatabaseConnection {
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return Connection
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function disconnect() : static {
		return $this;
	}
	
	/**
	 * @param    string    $charset
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setCharset(string $charset) : static {
		return $this;
	}
	
	/**
	 * @param    int    $port
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setPort(int $port) : static {
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConnection() : static {
		return $this;
	}
}
