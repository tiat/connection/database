<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Query;

//
use SplObjectStorage;
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DatabaseQueryInterface {
	
	/**
	 * @param    string    $name
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : DatabaseQueryInterface;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string;
	
	/**
	 * Add multiple queries at once
	 *
	 * @param    iterable    $queries
	 * @param    bool        $override    Override existing query
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addQueries(iterable $queries, bool $override) : DatabaseQueryInterface;
	
	/**
	 * Add single query.
	 * Notice! Remember to prepare query with prepareQuery() if not already done.
	 *
	 * @param    string|int    $name
	 * @param    string        $query
	 * @param    bool          $override    Override existing query
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addQuery(string|int $name, string $query, bool $override) : DatabaseQueryInterface;
	
	/**
	 * Get all queries at once
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueries() : ?array;
	
	/**
	 * Get query by key
	 *
	 * @param    string|int    $name
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery(string|int $name) : mixed;
	
	/**
	 * Has there a query with given name
	 *
	 * @param    string|int    $name
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasQuery(string|int $name) : bool;
	
	/**
	 * Delete query by key
	 *
	 * @param    string|int    $name
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function removeQuery(string|int $name) : DatabaseQueryInterface;
	
	/**
	 * Reset all queries
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetQueries() : DatabaseQueryInterface;
	
	/**
	 * Prepare query and merge parameters to query
	 *
	 * @param    string    $query
	 * @param    array     $args
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function prepareQuery(string $query, array $args) : ?string;
	
	/**
	 * Get single query result by name
	 *
	 * @param    array    $result
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryResult(array $result) : DatabaseQueryInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryResult(string $name) : mixed;
	
	/**
	 * @return null|SplObjectStorage
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryResults() : ?SplObjectStorage;
	
	/**
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetQueryResult() : DatabaseQueryInterface;
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 * @param    array|string                   $query
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function runQuery(DatabaseConnectionInterface $connection, array|string $query) : bool;
	
	/**
	 * @param    mixed                          $value
	 * @param    DatabaseConnectionInterface    $connection
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function quoteValue(mixed $value, DatabaseConnectionInterface $connection) : mixed;
}
