<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Driver;

//
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Exception\RuntimeException;

use function register_shutdown_function;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDatabaseDriver implements DatabaseDriverInterface {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_driverName;
	
	/**
	 * @param    DatabaseConnectionInterface    $_connection
	 *
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private readonly DatabaseConnectionInterface $_connection) {
		//
		register_shutdown_function([$this, 'shutdown']);
		
		//
		$this->checkExtension();
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkExtension() : bool {
		return TRUE;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
	
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		return $this;
	}
	
	/**
	 * @return null|DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : ?DatabaseConnectionInterface {
		return $this->_connection ?? NULL;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string {
		//
		if(empty($this->_driverName)):
			$this->setName();
		endif;
		
		//
		return $this->_driverName;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name = __CLASS__) : DatabaseDriverInterface {
		//
		$this->_driverName = strtolower($name);
		
		//
		return $this;
	}
}
