<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Connection;

//
use Jantia\Plugin\Monitor\Std\MonitorTime;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\RuntimeException;
use Tiat\Connection\Database\Helper\SettingsHelper;
use Tiat\Connection\Database\Result\DatabaseResultInterface;
use Tiat\Connection\Stdlib\Connection\Connection as ConnectionTrait;
use Tiat\Standard\Config\ConfigInterface;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function get_debug_type;
use function hash;
use function hash_algos;
use function implode;
use function in_array;
use function method_exists;
use function register_shutdown_function;
use function serialize;
use function sprintf;
use function trim;
use function ucfirst;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDatabaseConnection implements DatabaseConnectionInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ConnectionTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use SettingsHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 *
	 */
	use MonitorTime;
	
	/**
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected mixed $_connection;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	protected string $_connectionParamsHash;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	protected string $_connectionParamsHashAlgo = 'sha256';
	
	/**
	 * @var ConfigInterface
	 * @since   3.0.0 First time introduced.
	 */
	private ConfigInterface $_connectionParams;
	
	/**
	 * @var null|DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	private ?DatabaseResultInterface $_connectionResult;
	
	/**
	 * Is connection params ok and other needed checks to open connection
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_isConnectionPrepared = FALSE;
	
	/**
	 * @var DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DatabaseSettingsInterface $_settings;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_namespace;
	
	/**
	 * How long it took to connect to the database
	 *
	 * @var int
	 * @since   3.1.0 First time introduced.
	 */
	private int $_connectionTime;
	
	/**
	 * @param    null|DatabaseSettingsInterface    $settings
	 * @param    null|iterable                     $options
	 * @param    mixed                             ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(?DatabaseSettingsInterface $settings = NULL, ?iterable $options = NULL, ...$args) {
		// Register shutdown function
		register_shutdown_function([$this, 'shutdown']);
		
		//
		if($settings !== NULL):
			$this->setSettings($settings);
		endif;
		
		//
		$this->setConnectionOptions($options);
		
		//
		if(isset($args['namespace'])):
			$this->setNamespace($args['namespace']);
		endif;
	}
	
	/**
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetSettings() : DatabaseConnectionInterface {
		//
		unset($this->_settings);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionParamsHash() : ?string {
		return $this->_connectionParamsHash;
	}
	
	/**
	 * @param    string    $hash
	 *
	 * @return $this
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionParamsHash(string $hash) : static {
		//
		if(! empty($hash = trim($hash))):
			$this->_connectionParamsHash = $hash;
		else:
			throw new InvalidArgumentException("Hash must have an value.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void {
		// Close the connection if it's open
		$this->disconnect();
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasConnection() : bool {
		return FALSE;
	}
	
	/**
	 * How long it took to connect to the database
	 *
	 * @return null|int
	 * @since   3.1.0 First time introduced.
	 */
	public function getConnectionTime() : ?int {
		return $this->_connectionTime ?? NULL;
	}
	
	/**
	 * How long it took toe connect to the database
	 *
	 * @param    int    $time
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	protected function _setConnectionTime(int $time) : static {
		//
		$this->_connectionTime = $time;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		//
		if(( $this->isInitCompleted() === FALSE ) && $this->prepareConnection($this->_getSettings()) === TRUE):
			$this->setInitCompleted(TRUE);
		endif;
		
		return $this;
	}
	
	/**
	 * @param    null|DatabaseSettingsInterface    $settings
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function prepareConnection(?DatabaseSettingsInterface $settings = NULL) : bool {
		//
		if($this->isConnectionPrepared() === TRUE):
			return TRUE;
		elseif($settings !== NULL):
			//
			// Generate hash from the checked params & set connection prepared.
			$this->setConnectionParamsHash($this->generateConnectionParamsHash($this->_getConnectionParams()))
			     ->setConnectionPrepared(TRUE);
			
			//
			return TRUE;
		endif;
		
		// Throw an error if this line is reached
		throw new RuntimeException("Can't establish a new connection with given params.");
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isConnectionPrepared() : bool {
		return $this->_isConnectionPrepared;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionPrepared(bool $status) : static {
		//
		$this->_isConnectionPrepared = $status;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $params
	 *
	 * @return string
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function generateConnectionParamsHash(array $params) : string {
		if(! empty($params)):
			return hash($this->getConnectionParamsHashAlgo(), serialize($params));
		else:
			throw new InvalidArgumentException("Params must have at least one value.");
		endif;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionParamsHashAlgo() : ?string {
		return $this->_connectionParamsHashAlgo;
	}
	
	/**
	 * @param    string    $algo
	 *
	 * @return $this
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionParamsHashAlgo(string $algo) : static {
		//
		if(in_array($algo, hash_algos(), TRUE)):
			$this->_connectionParamsHashAlgo = $algo;
		else:
			$msg = sprintf("Given algorithm is not supported. Use on of following algorithms: %s.",
			               implode(', ', hash_algos()));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionParams() : array {
		//
		$array = $this->_getConnectionParamValues();
		foreach($array as $val):
			// Set method name
			$method = 'get' . ucfirst($val);
			
			// Try get method values
			if(method_exists($this->_getSettings(), $method)):
				$value = $this->_getSettings()->$method();
			elseif(method_exists($this, $method = '_' . $method)):
				$value = $this->$method();
			endif;
			
			//
			if(! empty($value)):
				$result[] = $value;
			endif;
			
			// This is important!
			unset($value);
		endforeach;
		
		//
		return $result ?? [];
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionParamValues() : array {
		return ['username', 'password', 'database', 'hostname'];
	}
	
	/**
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getSettings() : DatabaseSettingsInterface {
		return $this->_settings;
	}
	
	/**
	 * @param    DatabaseSettingsInterface    $settings
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettings(DatabaseSettingsInterface $settings) : static {
		//
		$this->_settings = $settings;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		//
		$this->getConnection();
		
		return $this;
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : mixed {
		if(empty($this->_connection)):
			$this->setConnection();
		endif;
		
		//
		return $this->_connection ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setConnection() : static;
	
	/**
	 * @return $this
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnection() : static {
		// Check that init is completed
		if($this->isInitCompleted() === FALSE):
			$this->init();
		endif;
		
		// All should be ok so let's continue
		if($this->isConnectionPrepared() === TRUE):
			// Do NOT open the connection if params have been changed somehow and those have not been checked
			if(! empty($params = $this->_getConnectionParams()) &&
			   $this->getConnectionParamsHash() === $this->generateConnectionParamsHash($params)):
				//
				return $this->_setConnection();
			else:
				throw new RuntimeException("Database connection params has been changed after the check.");
			endif;
		endif;
		
		// Something is so wrong...
		throw new RuntimeException("Database connection params has not been checked.");
	}
	
	/**
	 * @return null|DatabaseResultInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionResult() : ?DatabaseResultInterface {
		return $this->_connectionResult ?? NULL;
	}
	
	/**
	 * @param    mixed    $result
	 *
	 * @return $this
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionResult(mixed $result) : static {
		//
		if(! $result instanceof DatabaseResultInterface):
			$msg = sprintf("Given arg is not '%s', %s given.", DatabaseResultInterface::class, get_debug_type($result));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		$this->_connectionResult = $result;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionResult() : static {
		//
		$this->_connectionResult = NULL;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getNamespace() : ?string {
		return $this->_namespace ?? NULL;
	}
	
	/**
	 * @param    string    $namespace
	 *
	 * @return DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setNamespace(string $namespace) : DatabaseConnectionInterface {
		//
		$this->_namespace = $namespace;
		
		//
		return $this;
	}
}
