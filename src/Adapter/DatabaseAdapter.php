<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter;

//
use Tiat\Connection\Database\Adapter\Connection\Mysqli\Connection as MysqliConnection;
use Tiat\Connection\Database\Adapter\Connection\Pdo\Connection as PdoConnection;
use Tiat\Connection\Database\Adapter\Connection\Postgres\Connection as PostgresConnection;
use Tiat\Connection\Database\Adapter\Driver\DatabaseDriverInterface;
use Tiat\Connection\Database\Adapter\Driver\Mysqli\Driver as Mysqli;
use Tiat\Connection\Database\Adapter\Driver\Pdo\Driver as Pdo;
use Tiat\Connection\Database\Adapter\Driver\Postgres\Driver as Postgres;
use Tiat\Connection\Database\Exception\InvalidArgumentException;

use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DatabaseAdapter extends AbstractDatabaseAdapter {
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		// Create driver
		$this->createDriver();
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $driver
	 *
	 * @return DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _createDriver(string $driver) : DatabaseDriverInterface {
		//
		if(! empty($driver)):
			//
			return match ( $driver ) {
				self::DRIVER_MARIADB, self::DRIVER_MYSQL, self::DRIVER_MYSQLI => new Mysqli(new MysqliConnection($this->_getSettings(),
				                                                                                                 $this->getConnectionOptions())),
				self::DRIVER_POSTGRES => new Postgres(new PostgresConnection($this->_getSettings(),
				                                                             $this->getConnectionOptions())),
				self::DRIVER_PDO => new Pdo(( new PdoConnection($this->_getSettings(),
				                                                $this->getConnectionOptions()) )),
				default => throw new InvalidArgumentException(sprintf("Given driver %s is not supported.", $driver))
			};
		endif;
		
		//
		throw new InvalidArgumentException("Driver name can't be empty.");
	}
}
