<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Helper;

/**
 * @version 3.0.1
 * @since   3.0.0 First time introduced.
 */
trait SettingsHelper {
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_connectionOptions;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_initCompleted = FALSE;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionOptions() : ?array {
		return $this->_connectionOptions ?? NULL;
	}
	
	/**
	 * @param    NULL|array    $options
	 *
	 * @return SettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionOptions(?array $options = NULL) : SettingsInterface {
		//
		if(! empty($options)):
			$this->_connectionOptions = $options;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * As default this is empty.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionOptionsDefault() : array {
		return [];
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isInitCompleted() : bool {
		return $this->_initCompleted;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return SettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setInitCompleted(bool $status) : SettingsInterface {
		//
		$this->_initCompleted = $status;
		
		//
		return $this;
	}
}
