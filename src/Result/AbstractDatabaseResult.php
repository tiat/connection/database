<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Result;

//
use Countable;
use Iterator;
use Tiat\Connection\Database\Exception\DomainException;
use Tiat\Connection\Database\Exception\InvalidArgumentException;

use function array_combine;
use function implode;
use function in_array;
use function register_shutdown_function;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDatabaseResult implements Countable, Iterator, DatabaseResultInterface {
	
	/**
	 * Current row number in result
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	protected int $_position = 0;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_resultMode;
	
	/**
	 * Buffer data flag
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_buffered = TRUE;
	
	/**
	 * Buffered data
	 *
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	private mixed $_bufferedData;
	
	/**
	 * Has data already been fetch from backend
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_bufferedCompleted = FALSE;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_currentData;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_currentCompleted = FALSE;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_currentAllDataCompleted = FALSE;
	
	/**
	 * @var array
	 */
	private array $_fieldInfo;
	
	/**
	 * @param    string    $_name
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private string $_name) {
		register_shutdown_function([$this, 'shutdown']);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
	
	}
	
	/**
	 * Return the result object
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function getResult() : mixed;
	
	/**
	 * @return AbstractDatabaseResult
	 * @since   3.0.0 First time introduced.
	 */
	public function resetBufferedData() : static {
		//
		unset($this->_bufferedData);
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function isResult() : bool;
	
	/**
	 * @return null|array|bool
	 * @since   3.0.0 First time introduced.
	 */
	public function current() : array|bool|null {
		return $this->row($this->_position);
	}
	
	/**
	 * @param    int    $number
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function row(int $number) : mixed {
		return $this->_getResult($this->key());
	}
	
	/**
	 * @param    int    $row
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getResult(int $row) : mixed {
		//
		if($this->isBuffered() === TRUE):
			//
			if(empty($this->getBufferedData())):
				$this->setBufferedData($this->_getAllData());
			endif;
			
			//
			if(! empty($data = $this->getBufferedData())):
				return $this->_fetchDataRow($data, $row);
			endif;
			
			//
			return NULL;
		endif;
		
		//
		if($this->_checkCurrentData() === FALSE):
			//
			if(! empty($data = $this->_fetchNextRow())):
				$this->setCurrentData($data);
			endif;
			
			//
			return $this->setCurrentCompleted(TRUE)->getCurrentData() ?? [];
		elseif($this->getCurrentAllDataCompleted() === TRUE):
			//
			if(! empty($data = $this->getCurrentData())):
				return $this->_fetchDataRow($data, $row);
			endif;
			
			//
			return NULL;
		endif;
		
		// Current data has only one row content
		return $this->getCurrentData() ?? [];
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isBuffered() : bool {
		return $this->_buffered;
	}
	
	/**
	 * @param    bool    $buffered
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setBuffered(bool $buffered) : void {
		$this->_buffered = $buffered;
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getBufferedData() : mixed {
		return $this->_bufferedData ?? NULL;
	}
	
	/**
	 * @param    mixed    $data
	 *
	 * @return AbstractDatabaseResult
	 * @since   3.0.0 First time introduced.
	 */
	public function setBufferedData(mixed $data) : static {
		//
		$this->_bufferedData = $data;
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getAllData() : array {
		// Fetch all data from backend
		if($this->isBuffered() === TRUE && $this->_checkBufferedData() === FALSE):
			// Get data from backend if not tried already
			$this->setBufferedData($this->_fetchAllData())->setBufferedCompleted(TRUE);
			
			//
			return $this->getBufferedData() ?? [];
		endif;
		
		// If buffered data is already fetched, return it
		if($this->_checkCurrentAllData() === FALSE && ( $data = $this->_fetchAllData() ) !== NULL):
			$this->setCurrentData($data)->setCurrentCompleted(TRUE)->setCurrentAllDataCompleted(TRUE);
		endif;
		
		//
		return $this->getCurrentData() ?? [];
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkBufferedData() : bool {
		return ! ( $this->isBufferedCompleted() === FALSE && empty($this->getBufferedData()) );
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isBufferedCompleted() : bool {
		return $this->_bufferedCompleted;
	}
	
	/**
	 * @param    bool    $buffered
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setBufferedCompleted(bool $buffered) : void {
		$this->_bufferedCompleted = $buffered;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _fetchAllData() : ?array;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkCurrentAllData() : bool {
		return ! ( $this->getCurrentAllDataCompleted() === FALSE && empty($this->getCurrentData()) );
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurrentAllDataCompleted() : bool {
		return $this->_currentAllDataCompleted;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return AbstractDatabaseResult
	 * @since   3.0.0 First time introduced.
	 */
	public function setCurrentAllDataCompleted(bool $status) : static {
		//
		$this->_currentAllDataCompleted = $status;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurrentData() : ?array {
		return $this->_currentData ?? NULL;
	}
	
	/**
	 * @param    array    $data
	 *
	 * @return AbstractDatabaseResult
	 * @since   3.0.0 First time introduced.
	 */
	public function setCurrentData(array $data) : static {
		//
		$this->_currentData = $data;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $data
	 * @param    int      $row
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	private function _fetchDataRow(array $data, int $row) : mixed {
		return $data[$row] ?? [];
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkCurrentData() : bool {
		return ! ( $this->getCurrentCompleted() === FALSE && empty($this->getCurrentData()) );
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurrentCompleted() : bool {
		return $this->_currentCompleted;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return AbstractDatabaseResult
	 * @since   3.0.0 First time introduced.
	 */
	public function setCurrentCompleted(bool $status) : static {
		//
		$this->_currentCompleted = $status;
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _fetchNextRow() : array;
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function key() : int {
		return $this->_position;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getFieldInfo() : ?array {
		return $this->_fieldInfo ?? NULL;
	}
	
	/**
	 * @param    array    $fields
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setFieldInfo(array $fields) : static {
		//
		$this->_fieldInfo = $fields;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetFieldInfo() : static {
		//
		unset($this->_fieldInfo);
		
		//
		return $this;
	}
	
	/**
	 * @return void
	 * @throws DomainException
	 * @since   3.0.0 First time introduced.
	 */
	public function next() : void {
		//
		if($this->_position < ( $this->count() - 1 )):
			++$this->_position;
		else:
			$msg = sprintf("There are no more data left in result. Total rows in result is %u.", $this->_position + 1);
			throw new DomainException($msg);
		endif;
		
		//
		$this->_adjustResultPointer();
	}
	
	/**
	 * @return void
	 * @throws DomainException
	 * @since   3.0.0 First time introduced.
	 */
	private function _adjustResultPointer() : void {
		if($this->isBuffered() === FALSE && $this->_setRow($this->_position) === FALSE):
			throw new DomainException("Can't adjust the result pointer.");
		endif;
	}
	
	/**
	 * Set pointer in backend to given position
	 *
	 * @param    int    $row
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setRow(int $row) : bool;
	
	/**
	 * @return void
	 * @throws DomainException
	 * @since   3.0.0 First time introduced.
	 */
	public function rewind() : void {
		//
		if($this->_position > 0):
			--$this->_position;
		else:
			$msg =
				sprintf("There are no more data left in result. You are currently in row %u which is the first row in result.",
				        $this->_position + 1);
			throw new DomainException($msg);
		endif;
		
		//
		$this->_adjustResultPointer();
	}
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getResultMode() : ?int {
		return $this->_resultMode ?? NULL;
	}
	
	/**
	 * @param    int    $mode
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setResultMode(int $mode) : void {
		if($this->_checkResultMode($mode) === TRUE):
			$this->_resultMode = $mode;
		endif;
	}
	
	/**
	 * Check that given result mode is ok
	 *
	 * @param    int    $mode
	 *
	 * @return bool
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkResultMode(int $mode) : bool {
		//
		if(in_array($mode, ( $list = (array)$this->_validResultMode() ), TRUE)):
			return TRUE;
		endif;
		
		//
		$msg = sprintf("Result mode is not in allowed list: '%s', got '%u'.", implode(', ', $list), $mode);
		throw new InvalidArgumentException($msg);
	}
	
	/**
	 * Return valid result mode(s)
	 *
	 * @return int|array
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _validResultMode() : int|array;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function valid() : bool {
		return ( ! empty($this->row($this->_position)) );
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : static {
		//
		$this->_name = $name;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string {
		return $this->_name;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function toArray() : ?array {
		return $this->_getAllData();
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : bool {
		return FALSE;
	}
	
	/**
	 * @param    array    $result
	 * @param    array    $headers
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _fetchHeaders(array $result, array $headers) : array {
		return array_combine($headers, $result);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setRowReset() : void {
		$this->setCurrentCompleted(FALSE)->setCurrentAllDataCompleted(FALSE)->resetCurrentData();
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCurrentData() : static {
		//
		unset($this->_currentData);
		
		//
		return $this;
	}
}
