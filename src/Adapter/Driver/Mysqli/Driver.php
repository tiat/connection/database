<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Driver\Mysqli;

//
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Adapter\Connection\Mysqli\Connection;
use Tiat\Connection\Database\Adapter\Driver\AbstractDatabaseDriver;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\RuntimeException;

use function extension_loaded;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Driver extends AbstractDatabaseDriver {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int RESULT_STORE = MYSQLI_STORE_RESULT;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int RESULT_USE = MYSQLI_USE_RESULT;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_resultMode = self::RESULT_STORE;
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(DatabaseConnectionInterface $connection = new Connection()) {
		parent::__construct($connection);
	}
	
	/**
	 * @return bool
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function checkExtension() : bool {
		//
		if(! extension_loaded('mysqlnd')):
			throw new RuntimeException("Mysqlnd extension is required but the extension is not loaded.");
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getResultMode() : int {
		return $this->_resultMode;
	}
	
	/**
	 * Use MYSQLI_STORE_RESULT or MYSQLI_USE_RESULT. (Default MYSQLI_STORE_RESULT)
	 *
	 * @param    int    $mode
	 *
	 * @return $this
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function setResultMode(int $mode) : static {
		if($mode === self::RESULT_STORE || $mode === self::RESULT_USE):
			$this->_resultMode = $mode;
		else:
			$msg = sprintf("Arg value must be either %u or %u, %u given.", self::RESULT_STORE, self::RESULT_USE, $mode);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 * @param              $type
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function formatParameterName(string $name, $type = NULL) : string {
		return '?';
	}
}
