<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Result\Mysqli;

//
use Generator;
use mysqli;
use mysqli_result as MysqliResult;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Tiat\Connection\Database\Exception\RuntimeException;
use Tiat\Connection\Database\Result\AbstractDatabaseResult;

use function get_debug_type;
use function is_bool;
use function method_exists;
use function sprintf;

/**
 * @version 3.0.1
 * @since   3.0.0 First time introduced.
 */
class Result extends AbstractDatabaseResult {
	
	/**
	 * @var mysqli
	 * @since   3.0.0 First time introduced.
	 */
	private mixed $_result;
	private array $_data;
	
	/**
	 * @param    string        $name
	 * @param    object        $_connection
	 * @param    null|array    $_hashData
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string $name, private readonly object $_connection, private readonly ?array $_hashData = NULL) {
		//
		parent::__construct($name);
		
		//
		$this->setResultMode(MYSQLI_ASSOC);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
		//
		if(! empty($this->_result) && method_exists($this->_result, 'free')):
			$this->_result->free();
		endif;
		
		//
		$this->resetBufferedData();
		$this->resetCurrentData();
	}
	
	/**
	 * @return int
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	public function count() : int {
		if(( $e = $this->getResult() ) !== NULL):
			return $e->num_rows ?? 0;
		endif;
		
		//
		throw new RuntimeException("No database result available.");
	}
	
	/**
	 * @return null|MysqliResult|bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : MysqliResult|bool|null {
		return $this->_result ?? NULL;
	}
	
	/**
	 * Set $result as instance of mysqli_result
	 *
	 * @param    mixed    $result
	 * @param    bool     $multi
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 * @see     https://www.php.net/manual/en/mysqli.multi-query.php
	 */
	public function setResult(mixed $result, bool $multi = FALSE) : static {
		// With multi_query the $result can be boolean true/false
		if(! empty($result) || is_bool($result)):
			//
			$this->_result = $result;
			
			// If multi query is enabled, the result is expected to be a mysqli_result object
			if($multi === TRUE):
				// Avoid command out of sync error with this line
				$this->_fetchMultiQuery();
			endif;
		else:
			$msg = sprintf("Variable must be instance of mysqli_result, got '%s'.", get_debug_type($result));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return Result
	 * @since 3.1.0 First time introduced.
	 */
	protected function _fetchMultiQuery() : static {
		// Fetch rows from multiple queries and return them as a generator
		if(! empty($c = $this->_getConnection()) && $c instanceof mysqli && $c->more_results() === TRUE):
			//
			$result = [];
			
			// Reset counter so it can be matched against the hashdata array
			$counter = 0;
			
			//
			do {
				if($data = $c->store_result()):
					// Get fields info
					[$info, $headers] = $this->_fetchInfo($data);
					$this->setFieldInfo($info);
					
					// Use generator (using less memory and works better with large datasets)
					// Use hashdata string as a key to detect the SQL query result in application
					foreach($this->_fetchRow($data) as $row):
						// Always generate multiple rows
						// Application must handle the row data appropriately
						$result[$this->_getHashdata($counter)][] = $this->_fetchHeaders($row, $headers);
					endforeach;
				endif;
				
				// Increment counter for hash data matching
				$counter++;
			} while($c->next_result() === TRUE);
			
			//
			$this->_setResultData($result);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return object
	 * @since   3.1.0 First time introduced.
	 */
	private function _getConnection() : object {
		return $this->_connection;
	}
	
	/**
	 * @param    MysqliResult    $e
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _fetchInfo(MysqliResult $e) : ?array {
		// Fetch fields info
		if(! empty($info = $e->fetch_fields())):
			// Get headers
			foreach($info as $key => $val):
				$headers[$key] = $val->name;
			endforeach;
			
			// Return info and headers
			return [$info, $headers ?? []];
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    MysqliResult    $e
	 *
	 * @return Generator
	 * @since   3.0.0 First time introduced.
	 */
	protected function _fetchRow(MysqliResult $e) : Generator {
		// Fetch rows
		while($row = $e->fetch_row()):
			yield $row;
		endwhile;
	}
	
	/**
	 * @param    int    $id
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getHashdata(int $id) : ?string {
		return $this->_hashData[$id] ?? NULL;
	}
	
	/**
	 * @param    mixed    $data
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	private function _setResultData(mixed $data) : static {
		//
		$this->_data = $data;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isResult() : bool {
		return ( $this->_getConnection()->field_count > 0 );
	}
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getAffectedRows() : ?int {
		return $this->_getConnection()->affected_rows ?? NULL;
	}
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function lastInserted() : ?int {
		return $this->_getConnection()->insert_id ?? NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Added result free
	 */
	protected function _fetchAllData() : ?array {
		//
		try {
			// Check local cache (if multi query used)
			if(( $data = $this->getResultData() ) !== NULL):
				return $data;
			// Fetch all rows
			elseif(! empty($e = $this->getResult())):
				// Get fields info
				[$info, $headers] = $this->_fetchInfo($e);
				$this->setFieldInfo($info);
				
				// Use generator (using less memory and works better with large datasets)
				foreach($this->_fetchRow($e) as $row):
					$result[] = $this->_fetchHeaders($row, $headers);
				endforeach;
			elseif(! is_bool($e)):
				// Multi query is bool so prevent that handling
				throw new RuntimeException("No database result available.");
			endif;
		} catch(RuntimeException $e) {
			throw new RuntimeException($e->getMessage());
		}
		
		//
		return $result ?? NULL;
	}
	
	public function getResultData() : mixed {
		return $this->_data ?? NULL;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _validResultMode() : array {
		return [MYSQLI_ASSOC, MYSQLI_NUM, MYSQLI_BOTH];
	}
	
	/**
	 * @return array
	 * @throws RuntimeException
	 * @since   3.0.0 First time introduced.
	 */
	protected function _fetchNextRow() : array {
		//
		if(( $e = $this->getResult() ) !== NULL):
			return $e->fetch_assoc();
		endif;
		
		//
		throw new RuntimeException("No database result available.");
	}
	
	/**
	 * @param    int    $row
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setRow(int $row) : bool {
		// Move data pointer
		if($this->isBuffered() === FALSE && ( $e = $this->getResult() ) !== NULL && $e->data_seek($row) === TRUE):
			// Reset current data flags
			$this->_setRowReset();
			
			//
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
}
