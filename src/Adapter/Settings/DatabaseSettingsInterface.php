<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Settings;

//
use Tiat\Connection\Database\Adapter\DatabaseInterface;

/**
 * @version 3.0.1
 * @since   3.0.0 First time introduced.
 */
interface DatabaseSettingsInterface extends DatabaseInterface {
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriver() : ?string;
	
	/**
	 * @param    string    $driver
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setDriver(string $driver) : DatabaseSettingsInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUsername() : ?string;
	
	/**
	 * @param    string    $username
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setUsername(string $username) : DatabaseSettingsInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getPassword() : ?string;
	
	/**
	 * @param    string    $password
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPassword(string $password) : DatabaseSettingsInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHostname() : ?string;
	
	/**
	 * @param    string    $hostname
	 * @param    bool      $convert    Convert DNS hostname to IP address
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setHostname(string $hostname, bool $convert = FALSE) : DatabaseSettingsInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDatabase() : ?string;
	
	/**
	 * @param    string    $database
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDatabase(string $database) : DatabaseSettingsInterface;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCharset() : string;
	
	/**
	 * @param    string    $charset
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCharset(string $charset) : DatabaseSettingsInterface;
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getPort() : int;
	
	/**
	 * @param    int    $port
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPort(int $port) : DatabaseSettingsInterface;
	
	/**
	 * @param    string    $name
	 * @param    mixed     $value
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function checkSettingsValue(string $name, mixed $value) : DatabaseSettingsInterface;
	
	/**
	 * Get settings example for Laminas\Db\Adapter\Adapter
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfigArray() : ?array;
	
	/**
	 * Convert internal driver name to common (mariadb = mysqli)
	 *
	 * @param    string    $driver
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function convertDriverName(string $driver) : string;
}
