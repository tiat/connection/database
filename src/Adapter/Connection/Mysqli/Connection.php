<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Connection\Mysqli;

//
use Exception;
use mysqli;
use Tiat\Connection\Database\Adapter\Connection\AbstractDatabaseConnection;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Adapter\Settings\Mysqli\DatabaseSettingsMysqliInterface;
use Tiat\Connection\Database\Exception\DomainException;
use Tiat\Connection\Database\Exception\RuntimeException;

use function mysqli_report;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Connection extends AbstractDatabaseConnection {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DATABASE_CHARSET = 'utf8mb4';
	
	/**
	 * @param    NULL|DatabaseSettingsInterface    $settings
	 * @param    NULL|iterable                     $options
	 * @param                                      ...$args
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(?DatabaseSettingsInterface $settings = NULL, ?iterable $options = NULL, ...$args) {
		// Call parent constructor
		parent::__construct($settings, $options, ...$args);
		
		// Set mysqli report settings
		// This should throw exceptions when errors occur and provide a detailed error message
		// Index errors are discarded because those will raise error with "select *" query
		mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX | MYSQLI_REPORT_STRICT);
	}
	
	/**
	 * @return null|mysqli
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : ?mysqli {
		//
		if(empty($this->_connection) || ! $this->_connection instanceof mysqli):
			$this->setConnection();
		endif;
		
		//
		return $this->_connection ?? NULL;
	}
	
	/**
	 * @return $this
	 * @throws DomainException
	 * @since   3.0.0 First time introduced.
	 */
	public function disconnect() : static {
		//
		if(! empty($this->_connection) && ( $this->_connection instanceof mysqli )):
			//
			if($this->_connection->close() === FALSE):
				throw new DomainException("Can't close the database connection");
			endif;
			
			// Remove the object anyway
			$this->_connection = NULL;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Extend base params with driver specific
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionParamValues() : array {
		return [...parent::_getConnectionParamValues(), ...['port', 'charset', 'socket']];
	}
	
	/**
	 * @return Connection
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConnection() : static {
		//
		if(empty($this->_connection)):
			try {
				$settings = $this->_getSettings();
				
				// Check if settings are provided and instance of DatabaseSettingsMysqliInterface
				if($settings instanceof DatabaseSettingsMysqliInterface):
					// Start monitoring the time for connection
					$this->setMonitoringTime(self::MONITORING_START);
					
					// Fetch driver specific settings
					$conn = @new mysqli($settings->getHostname(), $settings->getUsername(), $settings->getPassword(),
					                    $settings->getDatabase(), $settings->getPort(), $settings->getSocket());
					
					//
					if($conn instanceof mysqli):
						//
						$this->_connection = $conn;
						
						// Security: set the default character set!
						if($this->_connection->set_charset($this->_getSettings()->getCharset()) === FALSE):
							throw new RuntimeException($this->_connection->error);
						endif;
					elseif($this->_connection->connect_errno):
						throw new RuntimeException($this->_connection->connect_error);
					endif;
					
					// Stop monitoring the time for connection
					$this->setMonitoringTime(self::MONITORING_STOP);
					
					// Save connection details for debugging
					$this->_setConnectionTime(self::getMonitoringTime());
				else:
					throw new RuntimeException("No settings defined for connection.");
				endif;
			} catch(Exception $e) {
				throw new RuntimeException($e->getMessage());
			}
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return DatabaseSettingsInterface|DatabaseSettingsMysqliInterface
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _getSettings() : DatabaseSettingsInterface|DatabaseSettingsMysqliInterface {
		return parent::_getSettings();
	}
}
