<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Query;

//
use Laminas\Stdlib\ArrayUtils;
use SplObjectStorage;
use Tiat\Collection\Crypt\CryptoTrait;
use Tiat\Collection\Crypt\CryptoTraitInterface;
use Tiat\Connection\Database\Adapter\Connection\DatabaseConnectionInterface;
use Tiat\Connection\Database\Exception\InvalidArgumentException;
use Traversable;

use function array_key_exists;
use function count;
use function preg_match;
use function sprintf;
use function trim;
use function uniqid;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDatabaseQuery implements DatabaseQueryInterface, CryptoTraitInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoTrait;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_name;
	
	/**
	 * @var null|array
	 * @since   3.0.0 First time introduced.
	 */
	private ?array $_query;
	
	/**
	 * @var SplObjectStorage
	 * @since   3.0.0 First time introduced.
	 */
	private SplObjectStorage $_queryResult;
	
	/**
	 * @param    string                         $name
	 * @param    DatabaseConnectionInterface    $_connection
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string $name, private readonly DatabaseConnectionInterface $_connection) {
		$this->setName($name);
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string {
		return $this->_name;
	}
	
	/**
	 * Set name of the object
	 * Name can contain only alphanumeric character(s)
	 *
	 * @param    string    $name
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.0.1 Keep the name as it is, no lowercasing.
	 * @since   3.0.2 Use \w in preg_match() pattern, as it matches any word character, including alphanumeric characters and underscores.
	 */
	public function setName(string $name) : DatabaseQueryInterface {
		//
		if(! empty($name = trim($name)) && preg_match('/^\w+$/', $name)):
			$this->_name = $name;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    iterable    $queries
	 * @param    bool        $override
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addQueries(iterable $queries, bool $override = FALSE) : DatabaseQueryInterface {
		//
		if($queries instanceof Traversable):
			$queries = (array)ArrayUtils::iteratorToArray((array)$queries);
		endif;
		
		//
		foreach($queries as $name => $query):
			$this->addQuery($name, $query, $override);
		endforeach;
		
		//
		return $this;
	}
	
	/**
	 * @param    int|string    $name
	 * @param    string        $query
	 * @param    bool          $override
	 *
	 * @return DatabaseQueryInterface
	 * @throws InvalidArgumentException
	 * @since   3.0.0 First time introduced.
	 */
	public function addQuery(int|string $name, string $query, bool $override = FALSE) : DatabaseQueryInterface {
		// Generate hash from the query for the query key
		if(! empty($query)):
			//
			$tmp[$hash = $this->hashData($query)] = $query;
			
			// Check if key exists and override is allowed
			if(isset($this->_query[$name])):
				// Array exists so just add to the array
				if(array_key_exists($hash, $this->_query[$name])):
					if($override === TRUE):
						// Modify the hash so that the query can be executed multiple times
						$this->_query[$name][uniqid($hash . '_', TRUE)] = $query;
					else:
						$msg =
							sprintf("Query with name '%s' already exists with same SQL query and double execution is prevented. Add override=true if you want to execute query multiple times.",
							        $name);
						throw new InvalidArgumentException($msg);
					endif;
				else:
					$this->_query[$name] = [...$this->_query[$name], ...$tmp];
				endif;
			else:
				$this->_query[$name][$hash] = $query;
			endif;
			
			//
			unset($tmp, $hash);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueries() : ?array {
		return $this->_query ?? NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.0.1 First time introduced.
	 */
	public function hasQueries() : bool {
		return ! empty($this->_query) && count($this->_query) > 0;
	}
	
	/**
	 * @param    int|string    $name
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery(int|string $name) : mixed {
		return $this->_query[$name] ?? NULL;
	}
	
	/**
	 * @param    int|string    $name
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasQuery(int|string $name) : bool {
		return isset($this->_query[$name]);
	}
	
	/**
	 * @param    int|string    $name
	 *
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function removeQuery(int|string $name) : DatabaseQueryInterface {
		//
		if(isset($this->_query[$name])):
			unset($this->_query[$name]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return DatabaseQueryInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetQueries() : DatabaseQueryInterface {
		//
		$this->_query = NULL;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $query
	 * @param    array     $args
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function prepareQuery(string $query, array $args) : ?string {
		return sprintf($query, ...$args);
	}
	
	/**
	 * @TODO FIXME This method is not implemented yet.
	 *
	 * @param    string    $name
	 * @param    array     $params
	 *
	 * @return void
	 */
	public function addParameters(string $name, array $params) : void {
	
	}
	
	/**
	 * @TODO FIXME This method is not implemented yet.
	 *
	 * @param    string    $name
	 * @param    array     $params
	 *
	 * @return void
	 */
	public function getParameters(string $name, array $params) : void {
	
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryResult(string $name) : ?array {
		//
		if(! empty($this->_queryResult) && ( count($i = $this->_queryResult) ) > 0):
			//
			$i->rewind();
			
			// Build result array
			while($i->valid()):
				if($i->getInfo() === $name):
					$result[$i->getInfo()] = $i->current();
				endif;
				
				//
				$i->next();
			endwhile;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $result
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryResult(array $result) : static {
		//
		if(! empty($result)):
			//
			if(empty($this->_queryResult)):
				$this->resetQueryResult();
			endif;
			
			//
			foreach($result as $val => $obj):
				$this->_queryResult->offsetSet($obj, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetQueryResult() : static {
		//
		$this->_queryResult = new SplObjectStorage();
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryResults() : ?SplObjectStorage {
		return $this->_queryResult ?? NULL;
	}
	
	/**
	 * @param    DatabaseConnectionInterface    $connection
	 * @param    array|string                   $query
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function runQuery(DatabaseConnectionInterface $connection, array|string $query) : bool;
	
	/**
	 * @return null|DatabaseConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnection() : ?DatabaseConnectionInterface {
		return $this->_connection ?? NULL;
	}
	
	/**
	 * Check that connection (type) is valid
	 *
	 * @param    DatabaseConnectionInterface    $connection
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _checkConnection(DatabaseConnectionInterface $connection) : bool;
}
