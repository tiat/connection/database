<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Database\Adapter\Settings\Mysqli;

//
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DatabaseSettingsMysqliInterface extends DatabaseSettingsInterface {
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSocket() : ?string;
	
	/**
	 * @param    null|string    $socket
	 *
	 * @return DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSocket(?string $socket) : DatabaseSettingsInterface;
}
